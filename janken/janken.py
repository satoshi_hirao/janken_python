import random


def judgeHand(yourhand, cmp):
    judge = [['draw', 'win', 'lose'], [
        'win', 'draw', 'lose'], ['lose', 'win', 'draw']]
    return judge[yourhand][cmp]


def convertHand2Num(hand):
    if(hand == 'グー'):
        return 0
    if(hand == 'チョキ'):
        return 1
    if(hand == 'パー'):
        return 2
    return -1


def main():
    yourHand = ''
    handList = ['グー', 'チョキ', 'パー']
    while(yourHand not in handList):
        print("グーorパーorチョキを入力")
        yourHand = input()

    cmp = random.choice(handList)
    print("相手の手：{}".format(cmp))

    result = judgeHand(convertHand2Num(yourHand), convertHand2Num(cmp))
    if(result == "draw"):
        print(result)
        return

    print("you {}".format(result))
    return

if __name__ == '__main__':
    main()
